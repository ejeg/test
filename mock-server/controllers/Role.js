import RoleModel from '../models/Role.js';

class RoleController {
    initRouter(app, apiUrl) {
        app.get(`${apiUrl}/roles`, this.getAll.bind(this));
    }

    getAll(req, res) {
        this._send(res, 200, RoleModel.getAll());
    }

    _send(res, status, response) {
        res.status(status)
           .send(response);
    }
}

export default new RoleController();
