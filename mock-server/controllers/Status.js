import StatusModel from '../models/Status.js';

class StatusController {
    initRouter(app, apiUrl) {
        app.get(`${apiUrl}/statuses`, this.getAll.bind(this));
    }

    getAll(req, res) {
        this._send(res, 200, StatusModel.getAll());
    }

    _send(res, status, response) {
        res.status(status)
           .send(response);
    }
}

export default new StatusController();
