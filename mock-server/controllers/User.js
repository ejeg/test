import UserModel from '../models/User.js';

class UserController {
    initRouter(app, apiUrl) {
        app.get(`${apiUrl}/users`, this.getAll.bind(this));
        app.get(`${apiUrl}/users/:id`, this.getOne.bind(this));
        app.post(`${apiUrl}/users`, this.create.bind(this));
        app.put(`${apiUrl}/users/:id`, this.update.bind(this));
        app.delete(`${apiUrl}/users`, this.delete.bind(this));
    }

    getAll(req, res) {
        this._send(res, 200, UserModel.getAll());
    }

    getOne(req, res) {
        var r;

        if ( r = UserModel.getOne(req.params.id) ) {
            this._send(res, 200, r);
        } else {
            this._send(res, 404, { error: 'no such item' });
        }
    }

    create(req, res) {
        this._send(res, 200, UserModel.create(req.body));
    }

    update(req, res) {
        var r;

        if ( r = UserModel.update(req.params.id, req.body) ) {
            this._send(res, 200, r);
        } else {
            this._send(res, 404, { error: 'no such item' });
        }
    }

    delete(req, res) {
        if ( UserModel.delete(req.query.id) ) {
            this._send(res, 200, { success: true });
        } else {
            this._send(res, 404, { error: 'no such item' });
        }
    }

    _send(res, status, response) {
        res.status(status)
           .send(response);
    }
}

export default new UserController();
