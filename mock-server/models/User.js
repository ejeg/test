import uuid from 'uuid';
import moment from 'moment';

class User {
    constructor() {
        this.users = [
            {
                id: 1,
                name: 'USA',
                createdDate: moment().subtract(2, 'days'),
                agentId: 1040,
                status: { id: 1, title: 'Active' },
                role: { id: 1, title: 'Admin' }
            },
            {
                id: 2,
                name: 'Sweden',
                createdDate: moment().subtract(3, 'months'),
                agentId: 1041,
                status: { id: 2, title: 'Suspended' },
                role: { id: 2, title: 'Manager' }
            },
            {
                id: 3,
                name: 'Finland',
                createdDate: moment().subtract(1, 'day'),
                agentId: 2020,
                status: { id: 1, title: 'Active' },
                role: { id: 3, title: 'Agent' }
            }
        ];
    }

    getAll() {
        return this.users;
    }

    getOne(id) {
        return this.users.find(v => v.id == id) || false;
    }

    create(data) {
        const lastId = Math.max(...this.users.map(v => v.id));

        const newUser = {
            id: lastId + 1,
            name: data.name,
            agentId: data.agentId,
            status: data.status || {},
            role: data.role || {},
            createdDate: moment.now(),
        }

        this.users.push(newUser);

        return newUser;
    }

    update(id, data) {
        var el = this.users.find(v => v.id == id);

        if ( el ) {
            Object.assign(el, {
                name: data.name,
                agentId: data.agentId,
                status: data.status,
                role: data.role,
            });
        }

        return el || false;
    }

    delete(id) {
        var el = this.users.find(v => v.id == id);

        if ( el ) {
            this.users.splice(el, 1);
        }

        return !!el;
    }
}

export default new User();
