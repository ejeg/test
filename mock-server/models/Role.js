class Role {
    constructor() {
        this.roles = [
            { id: 1, title: 'Admin' },
            { id: 2, title: 'Manager' },
            { id: 3, title: 'Agent' }
        ];
    }

    getAll() {
        return this.roles;
    }
}

export default new Role();
