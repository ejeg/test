class Status {
    constructor() {
        this.statuses = [
            { id: 1, title: 'Active' },
            { id: 2, title: 'Suspended' }
        ];
    }

    getAll() {
        return this.statuses;
    }
}

export default new Status();
