import express from 'express';
import UserController from './controllers/User.js';
import RoleController from './controllers/Role.js';
import StatusController from './controllers/Status.js';

const app = express();
const port = 4000;
const apiUrl = '/api';

app.use(express.json());

app.get('/', (req, res) => {
    return res
        .status(200)
        .send('ok');
});

UserController.initRouter(app, apiUrl);
RoleController.initRouter(app, apiUrl);
StatusController.initRouter(app, apiUrl);

app.listen(port);

console.log(`Running dev mock server on port ${port}`);
