export const roleRelations = {
  belongsTo: {
    user: {
      foreignKey: 'roleId',
      localField: 'user'
    }
  }
}
