export const statusRelations = {
  belongsTo: {
    user: {
      foreignKey: 'statusId',
      localField: 'user'
    }
  }
}
