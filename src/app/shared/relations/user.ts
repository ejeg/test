export const userRelations = {
  hasOne: {
    role: {
      foreignKey: 'id',
      localField: 'role'
    },
    status: {
      foreignKey: 'id',
      localField: 'status'
    }
  }
}
