import { Schema } from 'js-data';
import { roleSchema } from './role';
import { statusSchema } from './status';

export const userSchema = new Schema({
  title: 'User',
  description: 'Schema for User records',
  type: 'object',
  properties: {
    id: { type: 'number' },
    name: { type: 'string' },
    lastName: { type: 'string' },
    email: { type: 'string' },
    createdDate: { type: 'string' },
    agentId: { type: 'number' },
    status: statusSchema,
    role: roleSchema
  }
});
