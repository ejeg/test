import { Schema } from 'js-data';

export const roleSchema = new Schema({
  title: 'User role',
  description: 'Schema for User role records',
  type: 'object',
  properties: {
    id: { type: 'number' },
    title: { type: 'string' }
  }
});
