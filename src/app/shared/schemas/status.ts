import { Schema } from 'js-data';

export const statusSchema = new Schema({
  title: 'User status',
  description: 'Schema for User status records',
  type: 'object',
  properties: {
    id: { type: 'number' },
    title: { type: 'string' }
  }
});
