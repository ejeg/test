import { Injectable } from '@angular/core';
import { StoreService } from '../../core/store.service';
import { Schema } from 'js-data';

import { userSchema } from '../schemas/user';
import { userRelations } from '../relations/user';

@Injectable()
export class UserStoreService extends StoreService {
  mapper: string = 'user';
  items: any[] = [];

  constructor() {
    super();

    this.store.defineMapper(this.mapper, {
      endpoint: 'users',
      schema: userSchema,
      validateOnSet: false
//    relations: userRelations
    });
  }

  assignModel(model) {
    return this.items = model;
  }

  fetchAll() {
    return this.store
      .findAll(this.mapper)
      .then( items => {
        this.items.push(...items);
      });
  }

  find(id: number):any {
    return this.items.find(u => u.id == id);
  }

  create(data) {
    return this.store
      .create(this.mapper, data, { noValidate: true })
        .then( (result) => {
          this.items.push(result);
        });
  }

  update(id, data) {
    return this.store.update(this.mapper, id, data, { noValidate: true });
  }
}
