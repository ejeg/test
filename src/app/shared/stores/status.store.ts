import { Injectable } from '@angular/core';
import { StoreService } from '../../core/store.service';
import { Schema } from 'js-data';

import { statusSchema } from '../schemas/status';
import { statusRelations } from '../relations/status';

@Injectable()
export class StatusStoreService extends StoreService {
  mapper: string = 'user';

  constructor() {
    super();

    this.store.defineMapper(this.mapper, {
      endpoint: 'statuses',
      schema: statusSchema,
//    relations: statusRelations
    });
  }

  findAll() {
    return this.store.findAll(this.mapper);
  }

  create(data) {
    return this.store.create(this.mapper, data);
  }

  update(id, data) {
    return this.store.update(this.mapper, id, data);
  }
}
