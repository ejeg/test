import { Injectable } from '@angular/core';
import { StoreService } from '../../core/store.service';
import { Schema } from 'js-data';

import { roleSchema } from '../schemas/role';
import { roleRelations } from '../relations/role';

@Injectable()
export class RoleStoreService extends StoreService {
  mapper: string = 'role';

  constructor() {
    super();

    this.store.defineMapper(this.mapper, {
      endpoint: 'roles',
      schema: roleSchema,
//    relations: userRelations
    });
  }

  findAll() {
    return this.store.findAll(this.mapper);
  }

  create(data) {
    return this.store.create(this.mapper, data);
  }

  update(id, data) {
    return this.store.update(this.mapper, id, data);
  }
}
