import { Component, Input, Output, EventEmitter, ElementRef, Renderer2, OnInit } from '@angular/core';

import { StatusStoreService } from '../../../shared/stores/status.store';

@Component({  
    selector: 'dialog-change-status',
    styleUrls: ['./dialog-change-status.component.less'],
    templateUrl: './dialog-change-status.component.html'
})
export class PageUsersDialogChangeStatusComponent implements OnInit {
  @Input()
  set params(params) {
    this.show = !!params;

    if ( params ) {
      this.renderer.setStyle(this.elRef.nativeElement, 'top', params.y + 'px');
      this.renderer.setStyle(this.elRef.nativeElement, 'left', params.x + 'px');

      this.userId = params.userId;
      this.statusId = params.statusId;
    }
  }
  @Output() onClose = new EventEmitter();

  statusOptions = [];
  show = false;
  userId = false;
  statusId = false;
  
  constructor(private elRef: ElementRef, private renderer: Renderer2, private statusStore: StatusStoreService) {
  }

  ngOnInit() {
    this.fetchStatuses();
  }

  fetchStatuses() {
    this.statusStore.findAll().then( data => {
      this.statusOptions.push(...data);
    })
  }

  setValue(v) {
    this.onClose.emit({userId: this.userId, status: v});
  }

  close() {
    this.onClose.emit();    
  }
}
