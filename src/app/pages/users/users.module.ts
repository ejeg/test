import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MomentModule } from 'angular2-moment';

import { UsersRoutingModule } from './users-routing.module';
import { PageUsersComponent } from './users.component';

import { PageUsersToolbarComponent } from './toolbar/toolbar.component';
import { PageUsersGridComponent } from './grid/grid.component';
import { PageUsersUserInfoComponent } from './userinfo/userinfo.component';

import { PageUsersDialogAddComponent } from './dialog-add/dialog-add.component';
import { PageUsersDialogChangeStatusComponent } from './dialog-change-status/dialog-change-status.component';

import { UserStoreService } from '../../shared/stores/user.store';
import { RoleStoreService } from '../../shared/stores/role.store';
import { StatusStoreService } from '../../shared/stores/status.store';

@NgModule({
  declarations: [
    PageUsersComponent,
    PageUsersToolbarComponent,
    PageUsersGridComponent,
    PageUsersUserInfoComponent,
    PageUsersDialogAddComponent,
    PageUsersDialogChangeStatusComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    HttpClientModule,
    MomentModule,
    UsersRoutingModule
  ],
  providers: [
    UserStoreService,
    RoleStoreService,
    StatusStoreService
  ]
})
export class UsersModule { }
