import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'users-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.less']
})
export class PageUsersGridComponent {
  @Input()
  set items(value) {
    this.allItems = this.showItems = value;
  }

  @Input()
  set filters(filter) {
    this.showItems = this.allItems;

    for(let k in filter) {
      if ( k == 'search' ) {
        this.showItems = this.allItems.filter(v => this.search(v, filter[k]));
      } else {
        this.showItems = this.showItems.filter(v => (v[k].id == filter[k]) || !filter[k]);
      }
    }
  }

  @Output() onChangeStatus = new EventEmitter();

  allItems: any[];
  showItems: any[];

  search(item, value): boolean {
    var fields = ['id', 'name', 'agentId'];

    return !!fields.filter(field => !!item.get(field) && item.get(field).toString().toLowerCase().includes(value.toLowerCase())).length;
  }

  editStatus($event, user) {
    this.onChangeStatus.emit({event: $event, user: user});
  }
}
