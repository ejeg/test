import { Component, OnInit } from '@angular/core';

import { DialogService } from "ng2-bootstrap-modal";

import { PageUsersDialogAddComponent } from './dialog-add/dialog-add.component';
import { PageUsersDialogChangeStatusComponent } from './dialog-change-status/dialog-change-status.component';

import { UserStoreService } from '../../shared/stores/user.store';

@Component({
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.less']
})
export class PageUsersComponent implements OnInit {
  users = [];
  filters = {};

  showAddUserDialog: boolean = false;
  showChangeStatusDialog: boolean = false;
  changeStatusDialogParams: any = false;

  constructor(private userStore: UserStoreService) {
  }

  filter(value) {
    this.filters = Object.assign({}, value);
  }

  ngOnInit() {
     this.userStore.assignModel(this.users);

     this.userStore.fetchAll();
  }

  addUser() {
    this.showAddUserDialog = true;
  }

  onAddUserDialogClose(result) {
    this.showAddUserDialog = false;

    if ( result ) {
      this.userStore.create(result);
    }
  }

  changeStatus($event) {
    this.changeStatusDialogParams = {
      x: $event.event.x,
      y: $event.event.y,
      userId: $event.user.id,
      statusId: $event.user.status.id,
    }
  }

  onChangeStatusDialogClose(result: any) {
    this.changeStatusDialogParams = null;

    if ( result ) {
      var user = this.userStore.find(result.userId);

      // p.s. right way is to update status "sub-model" first and apply it to the user
      Object.assign(user.status, {
        id: result.status.get('id'),
        title: result.status.get('title')
      });

      user.save();
    }
  }
}
