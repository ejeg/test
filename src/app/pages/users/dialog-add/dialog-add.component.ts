import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { RoleStoreService } from '../../../shared/stores/role.store';

@Component({  
    selector: 'dialog-add',
    styleUrls: ['./dialog-add.component.less'],
    templateUrl: './dialog-add.component.html'
})
export class PageUsersDialogAddComponent implements OnInit {
  @Input() visible: boolean = false;
  @Output() onClose = new EventEmitter();

  role: string;
  email: string;
  firstName: string;
  lastName: string;
  agentId: number;

  roleOptions: any[] = [];
  
  constructor(private roleStore: RoleStoreService) {
    this.reset();
  }

  ngOnInit() {
    this.fetchRoles();
  }

  fetchRoles() {
    this.roleStore.findAll().then( data => {
      this.roleOptions.push(...data);
    })
  }

  onRoleChange($event) {
    this.role = $event;
  }

  reset() {
    this.role = undefined,
    this.email = '',
    this.firstName = '',
    this.lastName = '',
    this.agentId = undefined;
  }

  apply() {
    var result = {
      role: this.role,
      email: this.email,
      name: this.firstName,
      lastName: this.lastName,
      agentId: this.agentId
    };

    this.reset();
    this.onClose.emit(result);
  }

  close() {
    this.reset();
    this.onClose.emit();
  }
}
