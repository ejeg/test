import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { RoleStoreService } from '../../../shared/stores/role.store';
import { StatusStoreService } from '../../../shared/stores/status.store';

@Component({
  selector: 'users-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.less']
})
export class PageUsersToolbarComponent implements OnInit {
  @Input() search: string = '';
  @Input() status = 0;
  @Input() role = 0;
  @Output() filtered = new EventEmitter();
  @Output() addUser = new EventEmitter();

  searchMode: boolean = false;
  filters: any[];

  roleOptions: any = [
    { id: 0, title: 'All' }
  ];

  statusOptions: any = [
    { id: 0, title: 'All' }
  ];

  constructor(private roleStore: RoleStoreService, private statusStore: StatusStoreService) {
    this.filters = [];
  }

  ngOnInit() {
    this.fetchRoles();
    this.fetchStatuses();
  }

  fetchRoles() {
    this.roleStore.findAll().then( data => {
      this.roleOptions.push(...data);
    })
  }

  fetchStatuses() {
    this.statusStore.findAll().then( data => {
      this.statusOptions.push(...data);
    })
  }

  onSearchClick() {
    this.searchMode = true;
  }

  cancelSearch() {
    this.searchMode = false;
    this.search = '';
    this.filter('search');
  }

  filter(field, value = '') {
    this.filters[field] = value;
    this.filtered.emit(this.filters);
  }

  onAddUserClick() {
    this.addUser.emit();
  }
}
