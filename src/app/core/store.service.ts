import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DataStore, Schema } from 'js-data';
import { HttpAdapter } from 'js-data-http';

@Injectable()
export class StoreService {
  apiUrl: string = '/api';
  store: any;

  constructor() {
    const httpAdapter = new HttpAdapter({
        basePath: this.apiUrl
    });

    this.store = new DataStore();

    this.store.registerAdapter('http', httpAdapter, { 'default': true });
  }

  findAll(mapper) {
    return this.store.findAll(mapper);
  }

  create(mapper, data) {
    return this.store.create(mapper, data);
  }

  update(mapper, id, data) {
    return this.store.update(mapper, id, data);
  }
}
